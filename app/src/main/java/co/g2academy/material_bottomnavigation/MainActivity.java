package co.g2academy.material_bottomnavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    Menu optionsMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigation.setSelectedItemId(R.id.item1);
        BadgeDrawable badge = bottomNavigation.getOrCreateBadge(R.id.item1);
        badge.setNumber(90);
        badge.setVisible(true);

        bottomNavigation.setOnNavigationItemSelectedListener(new
                                                                     BottomNavigationView.OnNavigationItemSelectedListener() {
                                                                         @Override
                                                                         public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                                                                             switch (item.getItemId()){
                                                                                 case R.id.item1:
                                                                                     Log.d("Item 1 ","Clicked");
                                                                                     return true;

                                                                                 case R.id.item2:
                                                                                     Log.d("Item 2 ","Clicked");
                                                                                     return true;

                                                                             }
                                                                             return true;
                                                                         }
                                                                     });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bottom_navigation_menu, menu);
        optionsMenu = menu;
        return true;
    }



}